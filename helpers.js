import axios from 'axios';

const instance = axios.create({
  timeout: 1800000, // 30 minutes in milliseconds
});

export async function processMessage(data) {
  const { method, url, customHeaders, queueName, postParams } = data;

  const logData = {
    date: new Date(),
    url,
    method,
    queueName,
  };

  const options = {
    method,
    url,
    headers: customHeaders,
  };

  if (method === 'POST' && postParams) {
    options.data = postParams;
  }

  try {
    await instance(options);
    logData.status = 200;
  } catch (error) {
    if (error.response) {
      if (error.response.headers) {
        console.log('Responseheaders : ' + JSON.stringify(error.response.headers));
      }

      if (error.response.status) {
        logData.status = error.response.status;
      }
    }
  } finally {
    console.log(JSON.stringify(logData));
  }
}
