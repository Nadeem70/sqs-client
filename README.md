## To Run SQS Client

1. Make sure NodeJs is installed
2. cd sqs-client
3. sudo npm install forever -g
4. npm install [This will install all the dependencies of SQS Client application]
5. cp config-sample.json config.json
6. Edit config.json with the appropriate SQS Url.
7. forever start index.js
   or
   node index.js


## Development
1. Install devDependencies
2. Check "npm run lint" before committing
