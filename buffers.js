import sqsPackage from 'sqs-consumer';
import { processMessage } from './helpers.js';

const BUFFER_LIMIT          = 100;      //no of items when fetching should stop
const PRIORITY_BUFFER_LIMIT = 150;      //no of items when priority fetching should stop

const apps = [];
const buffer = new Set();

let priorityProcessing = true;

export function createApp(queueUrl, isPriority = false) {
  const app = {
    isPriority,
    consumer: sqsPackage.Consumer.create({
      queueUrl,
      visibilityTimeout: 1800,    //30 minutes in seconds
      handleMessage: addToBuffer,
    }),
  };

  // General error handling
  app.consumer.on('error', ({ message }) => console.error(message));
  app.consumer.on('processing_error', ({ message }) => console.error(message));

  if (isPriority) {
    // on receiving a priority item
    app.consumer.on('message_received', () => {
      priorityProcessing = true;

      // pause non-priority queues
      commandApps('stop', false);
    });

    // on priority queue being empty
    app.consumer.on('empty', () => {
      priorityProcessing = false;

      // restart non priority queues
      if (buffer.size < BUFFER_LIMIT) {
        commandApps('start', false);
      }
    });
  }

  apps.push(app);
}

function addToBuffer(message) {
  const { id, data } = extract(message);

  // Message is invalid
  if (!id) {
    return;
  }
  // Message already in buffer
  else if (buffer.has(id)) {
    console.log('Skipping duplicate message: ' + id);
    return;
  }

  buffer.add(id);

  // pause non priority apps when this limit is reached
  if (buffer.size >= BUFFER_LIMIT) {
    commandApps('stop', false);
  }

  // pause priority apps when this higher limit is reached
  if (buffer.size >= PRIORITY_BUFFER_LIMIT) {
    commandApps('stop', true);
  }

  console.log('Processing: ' + id);

  // Do not await here as we want the addToBuffer to be synchronous
  // Otherwise the queue will wait for this to complete before fetching the next item
  processMessage(data).finally(() => {
    console.log('Completed: ' + id);
    buffer.delete(id);

    // start priority apps when the priority buffer has capacity to accomodate more requests
    if (buffer.size < PRIORITY_BUFFER_LIMIT) {
      commandApps('start', true);
    }

    // start other apps when the non priority buffer has capacity to accomodate more requests
    if (buffer.size < BUFFER_LIMIT && !priorityProcessing) {
      commandApps('start', false);
    }
  });
}

/**
 * Validates the message object and extracts the data and identifier to uniquely identify the message
 * If there is a custom deDuplicationId sent, then that is the identifier, else it is the MessageId
 *
 * @param {object} message
 */
function extract(message) {
  if (message.MessageId && message.Body) {
    const data = JSON.parse(message.Body);

    if (data.method && data.url && data.customHeaders) {
      return {
        id: data.MessageDeduplicationId ? data.MessageDeduplicationId : message.MessageId,
        data,
      };
    }
  }
}

/**
 * Generic function to run a command on queues
 * @param {String}  command     Either 'start' or 'stop'
 * @param {Boolean} isPriority  Whether this is to run for priority or non priority apps
 */
export function commandApps(command, isPriority) {
  apps
    .filter(app => app.isPriority === isPriority)
    .filter(app => command === 'start' ? !app.consumer.isRunning : app.consumer.isRunning)
    .forEach(app => app.consumer[command]());
}
