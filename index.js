import { readFile } from 'fs';
import { createApp, commandApps } from './buffers.js';

readFile('./config.json', (error, data) => {
  if (error) {
    throw error;
  }

  const { queues, priorityQueues } = JSON.parse(data);

  // We have multiple SQS queues to process, and will require an app for each queue
  // Standard queues
  queues.forEach(url => createApp(url));
  // Prioritiy queue. Halts execution of the other queues if it is processing
  priorityQueues.forEach(url => createApp(url, true));

  commandApps('start', false);    // Start non priority apps
  commandApps('start', true);     // Start priority apps
});
